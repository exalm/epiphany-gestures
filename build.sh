#!/bin/bash

# Downloading separately from the manifest because it takes forever
# to both download and build it, and even longer to clone the git repo
if [ ! -e 'WebKit-main.zip' ]; then
  wget https://github.com/WebKit/WebKit/archive/refs/heads/main.zip -O WebKit-main.zip
fi

flatpak-builder --repo=repo build org.gnome.Epiphany.Devel.json --force-clean
flatpak build-bundle repo ephy.flatpak org.gnome.Epiphany.Devel gestures
